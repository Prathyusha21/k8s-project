package main
import (
    "fmt"
    "log"
    "net/http"
)
func index(w http.ResponseWriter, r *http.Request) {
    w.Header().Set(
        "Content-Type",
        "text/html",
    )
    html :=
        `<doctype html>
        <html>
    <head>
        <title>Hello K8s!</title>
    </head>
    <body>
        <b>Hello k8s!</b>
        <p>
            <a href="/welcome">Welcome123</a> |  <a href="/health">Health</a>
        </p>
    </body>
</html>`
    fmt.Fprintf(w, html)
}
func welcome(w http.ResponseWriter, r *http.Request) {
    fmt.Fprintf(w, "Welcome K8 World!")
}
func health(w http.ResponseWriter, r *http.Request) {
    fmt.Fprintf(w, "K8 World Application is Healthy!")
}
func main() {
    mux := http.NewServeMux()
    mux.Handle("/", http.HandlerFunc(index))
    mux.Handle("/welcome", http.HandlerFunc(welcome))
    mux.Handle("/health", http.HandlerFunc(health))
    log.Println("Listening...")
    http.ListenAndServe(":8081", mux)
}
